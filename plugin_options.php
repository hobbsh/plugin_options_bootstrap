<?php

//do you need some data for your options page? put custom functions here


/*

HOW TO USE OPTIONS ARRAY

each option needs a name, default value, description, and input_type (dropdown or text)
dropdown options need a data field that takes a single dimensional associative array as its value

name - code friendly name of option
default - set the default value
desc - description of the option, will show up on the options page next to the option
input type - dropdown or text
data - single dimensional assoc. array as variable or create your own array within, see examples


*/


function set_options(){
	
	//call custom functions if you need special data (or not so special data…)
	$cat_data = plugin_get_categories();

	
	$options = array(
		'post_category' => array ( //option 'slug'
			'name' => 'timeline_post_category', 
			'default' => '0', 
			'desc' => 'Select a post category for your timeline:', 
			'input_type' => 'dropdown', 
			'data' => $cat_data //data should be single dimensional assoc array
			),
		'show_posts' => array ( 
			'name' => 'timeline_show_posts', 
			'default' => '5', 
			'desc' => 'How many posts do you want to show?', 
			'input_type' => 'text'
			),
		'include_images' => array ( 
			'name' => 'timeline_include_images', 
			'default' => 'no', 
			'desc' => 'Do you want to include featured image thumbnails?', 
			'input_type' => 'dropdown', 
			'data' => array( //manual dropdown options
				'yes' => 'yes', 
				'no' => 'no')
				),
		'post_order' => array ( 
			'name' => 'timeline_order_posts' , 
			'default' => 'DESC', 
			'desc' => 'How do you want to order your posts?', 
			'input_type' => 'dropdown', 
			'data' => array(
				'Ascending' => 'ASC', 
				'Descending' => 'DESC') 
			)
	);

	return $options;
	
}

//create settings page
function plugin_settings() {
	?>
		<div class="wrap">	
			<h2><?php _e('Plugin Settings', PLUGIN_NAME_UNIQUE); ?></h2>
		<?php
		if (isset($_GET['updated']) && $_GET['updated'] == 'true') {
			?>
			<div id="message" class="updated fade"><p><strong><?php _e('Settings Updated', PLUGIN_NAME_UNIQUE); ?></strong></p></div>
			<?php
		}
		?>
			<form method="post" action="<?php echo esc_url('options.php');?>">
				<div>
					<?php settings_fields('plugin-settings'); ?>
				</div>
				
				<?php
					$options = set_options();
					
					?>
				<table class="form-table">
				<?php foreach($options as $option){ ?>
					<?php 
						//if option type is a dropdown, do this
						if ( $option['input_type'] == 'dropdown'){ ?>
							<tr valign="top">
				        		<th scope="row"><?php _e($option['desc'], PLUGIN_NAME_UNIQUE); ?></th>
				        			<td><select id="<?php echo $option['name']; ?>" name="<?php echo $option['name']; ?>">
				        					<?php foreach($option['data'] as $opt => $value){ ?>
												<option <?php if(get_option($option['name']) == $value){ echo 'selected="selected"';}?> name="<?php echo $option['name']; ?>" value="<?php echo $value; ?>"><?php echo $opt ; ?></option>
												<? } //endforeach ?>
										</select>
									</td>
					        </tr>
				    <?php 
				    	//if option type is text, do this
				    	}elseif ( $option['input_type'] == 'text'){ ?>
				    		<tr valign="top">
				        		<th scope="row"><?php _e($option['desc'], PLUGIN_NAME_UNIQUE); ?></th>
				        			<td><input id="<?php echo $option['name']; ?>" name="<?php echo $option['name']; ?>" value="<?php echo get_option($option['name']); ?>" />
									</td>
					        </tr>
			     <?php 
			     		
			     		}else{} //endif
			     		
			     	} //endforeach ?>
			        
			    </table>
			    <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Update', PLUGIN_NAME_UNIQUE); ?>" /></p>
			</form>
		</div>
	<?php
}

//register settings loops through options
function timeline_register_settings()
{
	$options = set_options(); //get options array
	
	foreach($options as $option){
		register_setting('plugin-settings', $option['name']); //register each setting with option's 'name'
		
		if (get_option($option['name']) === false) {
			add_option($option['name'], $option['default'], '', 'yes'); //set option defaults
		}
	}

	if (get_option('plugin_promote_plugin') === false) {
		add_option('plugin_promote_plugin', '0', '', 'yes');
	}

}
add_action( 'admin_init', 'plugin_register_settings' );


//add settings page
function plugin_settings_page() {	
	add_options_page('Plugin Settings', 'Plugin Settings', 'manage_options', PLUGIN_NAME_UNIQUE, 'plugin_settings');
}
add_action("admin_menu", 'plugin_settings_page');

?>